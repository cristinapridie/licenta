var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var TemaSchema = new mongoose.Schema({
    idMaterie:String,
    name:String,
    dueDate:Date,
    path:String,
    
});

TemaSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model("Tema", TemaSchema);