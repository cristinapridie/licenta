var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var CursSchema = new mongoose.Schema({
    name:String,
    idMaterie:String,
    path:String,
    
});

CursSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model("Curs", CursSchema);