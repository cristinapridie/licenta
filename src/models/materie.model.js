var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var MaterieSchema = new mongoose.Schema({
   
    name:String,
    color:String,
    userID:String,
});

MaterieSchema.plugin(passportLocalMongoose)

module.exports = mongoose.model("Materie", MaterieSchema);