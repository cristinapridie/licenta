let express = require("express");
var router = express.Router();
var User = require("../models/user.model");
var Materie = require("../models/materie.model")
var Curs = require("../models/curs.model")
var Tema = require("../models/tema.model")
var Fisier = require("../models/fisier.model")

var multer = require('multer');

let mongoose = require('mongoose');
var passportLocalMongoose = require("passport-local-mongoose");


const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client("89868546616-ngrj3hn43gcvp3p9b6ubcgj46nmmphct.apps.googleusercontent.com");

//router.use(express.static(path.join(__dirname,"u")))

router.get("/", function (req, res) {
    res.render('landing', { accessToken: null });
});

router.post("/tokensignin", function (req, res) {
    // console.log(req.body.idtoken);

    verify(req, res)

});
router.get("/getmateriiuser/:userid", function (req, res) {


    var userID = req.params.userid.substring(0, req.params.userid.length - 4)
    Materie.find({ "userID": userID }, function (err, materii) {


        if (err) {
            console.log(err);
            sendJsonResponse(res, false, "error", null, 500)
        } else {
            //console.log(materii)
            sendJsonResponse(res, true, "Succesfully retrieved materii", materii, 200)
            //res.render('landing', { accessToken: null });
        }
    })

})

router.post("/addmaterie", function (req, res) {
    var token = req.body.userid.substring(0, req.body.userid.length - 4)
    var nume = req.body.nume
    var color = req.body.culoare
    var materie = { name: nume, color: color, userID: token }
    Materie.create(materie, function (err, newlyCreated) {
        if (err) {
            console.log(err);
            sendJsonResponse(res, false, "error", null, 500)
        } else {
            //redirect back to campgrounds page
            console.log(newlyCreated);
            //sendJsonResponse(res, true, "Succesfully created", materie, 200)
            res.redirect("back")


        }
    });

    //console.log(req.body)
})
router.get("/getcursuri/:idMaterie", function (req, res) {


    var idMaterie = req.params.idMaterie
    Curs.find({ "idMaterie": idMaterie }, function (err, cursuri) {


        if (err) {
            console.log(err);
            sendJsonResponse(res, false, "error", null, 500)
        } else {
            //console.log(materii)
            sendJsonResponse(res, true, "Succesfully retrieved materii", cursuri, 200)
            //res.render('landing', { accessToken: null });
        }
    })

})
router.get("/getteme/:idMaterie", function (req, res) {


    var idMaterie = req.params.idMaterie
    Tema.find({ "idMaterie": idMaterie }, function (err, teme) {


        if (err) {
            console.log(err);
            sendJsonResponse(res, false, "error", null, 500)
        } else {
            console.log(teme)
            sendJsonResponse(res, true, "Succesfully retrieved teme", teme, 200)
            //res.render('landing', { accessToken: null });
        }
    })

})

router.delete("/deletematerie:idMaterie", function (req, res) {
    var id = req.body.idMaterie
    var myquery = { _id: id };
    Materie.deleteOne(myquery, function (err, newlyCreated) {
        if (err) {
            console.log(err);
            sendJsonResponse(res, false, "error", null, 500)
        } else {
            //redirect back to campgrounds page

            sendJsonResponse(res, true, "Succesfully deleted", myquery, 200)
            //window.location = "profile" + response;
            res.redirect("back")


        }
    });

    //console.log(req.body)
})
router.get("/file", function (req, res) {
    res.render("incarcaFisier", { accessToken: 1 })
})

var upload = require("express-fileupload")
router.use(upload())

router.post("/uploadFile", function (req, res) {

    var type = req.body.type
    var nume = req.body.nume
    var idMaterie = req.body.idMaterie

    if (type == "curs") {

        if (req.files) {
            var file = req.files.filename,
                filename = file.name;
            file.mv("./public/upload/" + filename, function (err) {
                if (err) {
                    console.log(err)
                    sendJsonResponse(res, false, "eroare", null, 500)

                }
                else {
                    res.redirect("./upload/" + filename)
                    var curs = { name: nume, idMaterie: idMaterie, path: "./public/upload/" + filename}
                    Curs.create(curs, function (err, newlyCreated) {
                        if (err) {
                            console.log(err);
                            //sendJsonResponse(res, false, "error", null, 500)
                        } else {
                            console.log(newlyCreated);
                            // res.redirect("back")
                            //sendJsonResponse(res, false, "Success", newlyCreated, 200)
                        }
                    });
                }
            })
        }
        else {
            sendJsonResponse(res, false, "eroare", null, 500)
        }
    } else if (type == "tema") {
        if (req.files) {
            var file = req.files.filename,
                filename = file.name;
            file.mv("./public/upload/" + filename, function (err) {
                if (err) {
                    console.log(err)
                    sendJsonResponse(res, false, "eroare", null, 500)
                    
                }
                else {
                    res.redirect("./upload/" + filename)


                    var tema = { name: nume, idMaterie: idMaterie, deadline: 0, path: "./public/upload/" + filename}
                    Tema.create(tema, function (err, newlyCreated) {
                        if (err) {
                            console.log(err);
                            // sendJsonResponse(res, false, "error", null, 500)
                        } else {
                            console.log(newlyCreated);
                            // res.redirect("back")
                            // sendJsonResponse(res, false, "Success", null, 200)
                        }
                    });
                }
            })
        }
        else {
            sendJsonResponse(res, false, "eroare", null, 500)
        }
    }


})

async function verify(req, res) {
    const ticket = await client.verifyIdToken({
        idToken: req.body.idtoken,
        audience: "89868546616-ngrj3hn43gcvp3p9b6ubcgj46nmmphct.apps.googleusercontent.com",
    });
    const payload = ticket.getPayload();
    const userid = payload['sub'];
    // If request specified a G Suite domain:
    //const domain = payload['hd'];

    saveToDB(ticket.payload.sub, ticket.payload.email, ticket.payload.name, res)
}


function saveToDB(sub, email, name, res) {
    console.log(sub + email + name)
    User.findOne({ "userId": sub }, function (err, user) {
        if (err || user == null || user == undefined) {
            var newUser = { userId: sub, email: email, name: name }
            // Create a new campground and save to DB

            User.create(newUser, function (err, newlyCreated) {
                if (err) {
                    console.log(err);
                    sendJsonResponse(res, false, "error", null, 500)
                } else {
                    //redirect back to campgrounds page
                    console.log(newlyCreated);
                    sendJsonResponse(res, true, "Succesfully created", sub, 200)
                }
            });

        } else {
            sendJsonResponse(res, true, "Succesfully logged in", sub, 200)
        }
    })


}
router.post("/api/saveusertodb", (req, res) => {

})

function sendJsonResponse(res, success, message, data, code) {
    var jsonResponse = {}
    jsonResponse["Success"] = success
    jsonResponse["message"] = message
    jsonResponse["data"] = data
    res.status(code).send(jsonResponse)
}

module.exports = router;