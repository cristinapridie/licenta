let express = require("express");
let app = express();
let bodyParser = require('body-parser');
let path = require('path');
let userRoute = require('./routes/user');

app.use(express.static('public'));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: false }));

let mongoose = require('mongoose');
var User = require("./models/user.model");
var Materie=require("./models/materie.model");
var Tema=require("./models/tema.model");



//const { OAuth2Client } = require('google-auth-library');
//const client = new OAuth2Client("89868546616-ngrj3hn43gcvp3p9b6ubcgj46nmmphct.apps.googleusercontent.com");

mongoose.connect("mongodb+srv://cristinaP:hL5dOprWWxOiDjUG@cluster0-05jai.mongodb.net/test?retryWrites=true&w=majority")
mongoose.set('useCreateIndex',true);


app.get("/profile:accessToken", function (req, res) {
    res.render('profile',{accessToken:req.params.accessToken});
})

app.use(function (req, res, next) {
    res.locals.currentUser = req.user;
    next();
});
app.use("/",userRoute);


//Handler for 404 resource not fount
app.use((req, res, next) => {
    res.status(400).sendFile(path.join(__dirname, '../public/error404.html'));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, process.env.IP, function () {
    console.log("E-learning web app server started on port " + PORT);
});

